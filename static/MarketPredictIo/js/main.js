$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
})

function ToggleLoadingWheel(){
    $("#loading-wheel").toggle();
}

function ToggleUploadOwnData(){
    if ($("#if-own-data").is(':checked')) {
        console.log("checked");
        $("#prepared-data-choice").css("display", "none");
        $("#own-data-choice").css("display", "block");
    }
    else
    {
        console.log("unchecked");
        $("#prepared-data-choice").css("display", "block");
        $("#own-data-choice").css("display", "none");
    }
}
