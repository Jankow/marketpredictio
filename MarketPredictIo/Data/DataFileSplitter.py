import split_folders


if __name__ == '__main__':
    split_folders.ratio('D:\\Projects\\MarketPredictIo\\MarketPredictIo\\Data\\', output="", seed=1337,
                        ratio=(.8, .1, .1))


# import shutil
# import os, sys, random
#
# path = 'D:\\Projects\\MarketPredictIo\\MarketPredictIo\\Data\\Stocks\\'
# files = []
# i = 0
# for r, d, f in os.walk(path):
#     for file in f:
#         if '.txt' in file:
#             subFolderName = ""
#             if i < 3:
#                 subFolderName = "Train"
#                 i += 1
#             elif i == 3:
#                 subFolderName = "Validation"
#                 i += 1
#             elif i == 4:
#                 subFolderName = "Test"
#                 i = 0
#             subFolderPath = os.path.join(path, subFolderName)
#             shutil.move(os.path.join(path, file), subFolderPath)
#
#
# # With Random:
# def splitDirs(files, dir1, dir2, ratio):
#     shuffled = files[:]
#     random.shuffle(shuffled)
#     num = round(len(shuffled) * ratio)
#     to_dir1, to_dir2 = shuffled[:num], shuffled[num:]
#     for d in dir1, dir2:
#         if not os.path.exists(d):
#             os.mkdir(d)
#     for file in to_dir1:
#         os.symlink(file, os.path.join(dir1, os.path.basename(file)))
#     for file in to_dir2:
#         os.symlink(file, os.path.join(dir2, os.path.basename(file)))
#
#
# if __name__ == '__main__':
#     if len(sys.argv) != 5:
#         sys.exit('Usage: {} files.txt dir1 dir2 ratio'.format(sys.argv[0]))
#     else:
#         files, dir1, dir2, ratio = sys.argv[1:]
#         ratio = float(ratio)
#         files = open(files).read().splitlines()
#         splitDirs(files, dir1, dir2, ratio)

# # Trying to do the reversal of above:
# for r, d, f in os.walk(path):
#     for directory in d:
#         if '.txt' in file:
#             subFolderName = ""
#             if i < 3:
#                 subFolderName = "Train"
#                 i += 1
#             elif i == 3:
#                 subFolderName = "Validation"
#                 i += 1
#             elif i == 4:
#                 subFolderName = "Test"
#                 i = 0
#             subFolderPath = os.path.join(path, subFolderName)
#             shutil.move(os.path.join(path, file), subFolderPath)

