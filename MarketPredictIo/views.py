# Create your views here.

from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render

from MarketPredictIo.Models import StandardReluNN
from MarketPredictIo.Models import ConvolutionalNN
from MarketPredictIo.Models import GatedRecurrentUnitNN
from MarketPredictIo.Models import ShortLongTermMemoryNN

from matplotlib.backends.backend_agg import FigureCanvasAgg

from datetime import datetime, timedelta

import os


def index(request):
    directory = "D:\\Projects\\MarketPredictIo\\MarketPredictIo\\Data\\temp\\test\\Stocks"
    csvList = []
    i = 0
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(".csv"):
                csvList.append(file)
                csvList[i] = csvList[i].replace(".csv", "").replace(".CSV", "").upper()
                i += 1
    dictOfTestData = {i: csvList[i] for i in range(0, len(csvList))}
    directory = "D:\\Projects\\MarketPredictIo\\MarketPredictIo\\Models\\Saved"
    jsonList = []
    i = 0
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(".json"):
                jsonList.append(file)
                jsonList[i] = jsonList[i].replace(".json", "")
                i += 1
    dictOfNNs = {i: jsonList[i] for i in range(0, len(jsonList))}
    # template = loader.get_template('index.html')
    # return HttpResponse(template.render())
    return render(request, 'index.html', {'testData': dictOfTestData, 'NNList': dictOfNNs})


def results(request):
    chosenAlgorithm = ''
    chosenData = ''
    uploadCheckbox = ''
    uploadFile = ''
    if request.method == "POST":
        # print(request.POST)
        # print("request.POST.get('algorithm'): " + request.POST.get('algorithm'))
        # print("request.POST.get('data'): " + request.POST.get('data'))
        # print("request.POST.get('upload'): " + str(request.POST.get('upload')))  # None or Checked
        # print("request.POST.get('uploadFile'): " + request.POST.get('uploadFile'))
        # chosenAlgorithm = request.POST.get('algorithm')
        # chosenData = request.POST.get('data')
        # uploadCheckbox = request.POST.get('upload')  # None or Checked
        # uploadFile = request.POST.get('uploadFile')

        # if request.POST.get('algorithm') == 'NeuralNetwork':
        # print("NeuralNetwork chosen.")
        if request.POST.get('teach_button'):
            number_of_epochs = int(request.POST.get('number-of-epochs'))
            model_name = request.POST.get('model-name')
            model_name = model_name.replace(" ", "_")
            start_date = request.POST.get('start-date')
            end_date = request.POST.get('end-date')
            print(start_date)
            if datetime.strptime(start_date, '%Y-%m-%d') > datetime(2017, 11, 10) \
                    or datetime.strptime(end_date, '%Y-%m-%d') > datetime(2017, 11, 10):
                return render(request, 'results.html', {'error': 'Too late date.'})
            if datetime.strptime(start_date, '%Y-%m-%d') < datetime(2000, 1, 1) \
                    or datetime.strptime(end_date, '%Y-%m-%d') < datetime(2000, 1, 1):
                return render(request, 'results.html', {'error': 'Too early date.'})
            model_graphic, plot_graphic, no_success = StandardReluNN.teach(number_of_epochs, model_name, start_date, end_date)
            if no_success == 4:
                return render(request, 'results.html', {'error': 'Too little stocks have enough records between dates.'})
            if no_success == 5:
                return render(request, 'results.html', {'error': 'Wrong data.'})
            return render(request, 'results.html', {'model': model_graphic.decode('utf8'),
                                                    'func': plot_graphic.decode('utf8')})
        elif request.POST.get('results_button'):
            model_name = request.POST.get('model-name')
            if request.POST.get('upload') == "Checked":
                chosen_data = None
                uploaded_data = request.FILES['uploadFile'] if 'uploadFile' in request.FILES else False
                if uploaded_data == False:
                    return render(request, 'results.html', {'error': 'No file uploaded.'})
                file_extension = os.path.splitext(uploaded_data.name)[1]
                if file_extension != '.csv':
                    return render(request, 'results.html', {'error': 'Wrong format of uploaded file.'})
            else:
                chosen_data = request.POST.get('data')
                uploaded_data = None
            # print(request.FILES)
            # print(request.FILES['uploadFile'])
            # print(uploaded_data)
            # print(chosen_data)
            if_validation_data = request.POST.get('evaluation-data') == "Checked"
            print(if_validation_data)
            # print(model_name + ", " + chosen_data)
            model_graphic, plot_graphic, no_success = StandardReluNN.generate_result(chosen_data,
                                                                                  model_name,
                                                                                  uploaded_data,
                                                                                  if_validation_data)
            if no_success == 1:
                return render(request, 'results.html', {'error': 'Not enough samples of data.'})
            if no_success == 2:
                return render(request, 'results.html', {'error': '"Close" and/or "Volume" column in the data file is missing.'})
            if no_success == 3:
                return render(request, 'results.html', {'error': 'Too many columns called "Close" and "Volume".'})
            return render(request, 'results.html', {'model': model_graphic.decode('utf8'),
                                                    'plot': plot_graphic.decode('utf8')})
        else:
            print("Neither button pressed?")

    #     elif request.POST.get('algorithm') == 'Convolutional':
    #         print("Convolutional chosen.")
    #         if request.POST.get('teach_button'):
    #             model_graphic, plot_graphic = ConvolutionalNN.teach()
    #             return render(request, 'results.html', {'model': model_graphic.decode('utf8'),
    #                                                     'func': plot_graphic.decode('utf8')})
    #         elif request.POST.get('results_button'):
    #             model_graphic, plot_graphic = ConvolutionalNN.generate_result(request.POST.get('data'))
    #             return render(request, 'results.html', {'model': model_graphic.decode('utf8'),
    #                                                     'plot': plot_graphic.decode('utf8')})
    #         else:
    #             print("Neither button pressed?")
    #
    #     elif request.POST.get('algorithm') == 'ShortLongTermMemory':
    #         print("ShortLongTermMemory chosen.")
    #         graphic = ShortLongTermMemoryNN.generate_result()
    #         return render(request, 'results.html', {'plot': graphic.decode('utf8')})
    #
    #     elif request.POST.get('algorithm') == 'GatedRecurrentUnit':
    #         print("GatedRecurrentUnit chosen.")
    #         graphic = GatedRecurrentUnitNN.generate_result()
    #         return render(request, 'results.html')
    #
    # else:
    #     print("Error occured. POST from 'See results' was sent as a GET. GET details:")
    #     print(request.GET)

    # serializer = SnippetSerializer(snippet, data=request.data)
    # if serializer.is_valid():
    #     serializer.save()
    #     data = serializer.data

    return render(request, 'results.html')

def wiki(request):
    # template = loader.get_template('index.html')
    # return HttpResponse(template.render())
    return render(request, 'wiki.html')
