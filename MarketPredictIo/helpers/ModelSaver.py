from tensorflow.python import keras, os
from io import *


def save(model, name_of_model):
    # serialize model to JSON
    model_json = model.to_json()
    with open(os.path.join("MarketPredictIo\\Models\\Saved", name_of_model + ".json"), "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(os.path.join("MarketPredictIo\\Models\\Saved", name_of_model + ".h5"))
    print("Saved model to disk")
    return


def load(name_of_model):
    # load json and create model
    json_file = open(os.path.join("MarketPredictIo\\Models\\Saved", name_of_model + ".json"), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = keras.models.model_from_json(loaded_model_json)
    # load weights into new model
    model.load_weights(os.path.join("MarketPredictIo\\Models\\Saved", name_of_model + ".h5"))
    print("Loaded model from disk")
    return model
