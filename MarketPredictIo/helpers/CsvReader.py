from pandas_datareader import data
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import urllib.request, json
import os
import numpy as np
import tensorflow as tf  # This code has been tested with TensorFlow 1.6
from tensorflow.python import keras
from sklearn.preprocessing import MinMaxScaler
import pydot
import graphviz
from IPython.display import SVG
from tensorflow.python.keras.utils import plot_model
# from keras.utils import model_to_dot

import PIL.Image
import io
from io import *
from django.shortcuts import render
from matplotlib import pylab
from pylab import *
import base64


def read_from_dir(directory, start_date, end_date):

    original_trainDir = os.path.join(directory, "train", "Stocks")
    original_valDir = os.path.join(directory, "val", "Stocks")

    trainDir = os.path.join(directory, "temp", "train", "Stocks")
    valDir = os.path.join(directory, "temp", "val", "Stocks")

    print("Start of removal")

    for root, dirs, files in os.walk(trainDir):
        for file in files:
            if file.endswith(".csv"):
                os.remove(os.path.join(trainDir, file))

    print("Removed train data")

    for root, dirs, files in os.walk(valDir):
        for file in files:
            if file.endswith(".csv"):
                os.remove(os.path.join(valDir, file))

    print("Removed val data")

    train_csv_list = []
    i = 0
    for root, dirs, files in os.walk(original_trainDir):
        for file in files:
            if file.endswith(".txt"):
                df = pd.read_csv(
                    os.path.join(original_trainDir, file),
                    delimiter=',',
                    usecols=['Date', 'Open', 'High', 'Low', 'Close', 'Volume'])
                df['Date'] = pd.to_datetime(df['Date'])
                mask = (df['Date'] > start_date) & (df['Date'] <= end_date)
                df = df.loc[mask]
                # print(df)
                df.drop(['Date', 'Open', 'High', 'Low'], axis=1, inplace=True)
                # df.pop('Date')
                # df.pop('Open')
                # df.pop('High')
                # df.pop('Low')
                # print(df)
                df_size = len(df.index)
                if df_size == 125:
                    train_csv_list.append(df)
                elif df_size > 125:
                    df = df.tail(125)  # or df.iloc[-125:]
                    train_csv_list.append(df)
                # print(len(csvList))
                # df.to_csv("MarketPredictIo\\Data\\temp\\train\\Stocks\\" + file + ".csv")
                i += 1

    print("Read train data into memory")

    val_csv_list = []
    i = 0
    for root, dirs, files in os.walk(original_valDir):
        for file in files:
            if file.endswith(".txt"):
                df = pd.read_csv(
                    os.path.join(original_valDir, file),
                    delimiter=',',
                    usecols=['Date', 'Open', 'High', 'Low', 'Close', 'Volume'])
                df['Date'] = pd.to_datetime(df['Date'])
                mask = (df['Date'] > start_date) & (df['Date'] <= end_date)
                df = df.loc[mask]
                df.drop(['Date', 'Open', 'High', 'Low'], axis=1, inplace=True)
                df_size = len(df.index)
                if df_size == 125:
                    val_csv_list.append(df)
                elif df_size > 125:
                    df = df.tail(125)  # or df.iloc[-125:]
                    val_csv_list.append(df)
                # print(len(csvList))
                # df.to_csv("MarketPredictIo\\Data\\temp\\train\\Stocks\\" + file + ".csv")
                i += 1

    print("Read val data into memory")

    if len(train_csv_list) < 40:
        print("Too little stocks have enough records between dates.")
        return None, None, 1

    if len(val_csv_list) < 5:
        print("Too little stocks have enough records between dates.")
        return None, None, 1

    print("Checked too little exceptions")

    i = 0
    for csv in train_csv_list:
        csv.to_csv(os.path.join(trainDir, "csv"+str(i)+".csv"))
        i += 1

    print("Saved prepared train data")

    i = 0
    for csv in val_csv_list:
        csv.to_csv(os.path.join(valDir, "csv"+str(i)+".csv"))
        i += 1

    print("Saved prepared val data")

    return train_csv_list, val_csv_list, 0


def read_prepared_data(directory):

    train_path = os.path.join(directory, "train", "Stocks")
    val_path = os.path.join(directory, "train", "Stocks")

    train_csv_list = []
    i = 0
    for root, dirs, files in os.walk(train_path):
        for file in files:
            if file.endswith(".csv"):
                # print(file)
                df = pd.read_csv(
                    os.path.join(train_path, file),
                    delimiter=',',
                    usecols=['Close', 'Volume']  # ['Date', 'Open', 'High', 'Low', 'Close', 'Volume'])
                )
                train_csv_list.append(df)
                if df.isnull().values.any():
                    print("Something is null in: {}".format(file))
                i += 1

    val_csv_list = []
    i = 0
    for root, dirs, files in os.walk(val_path):  # directory change to val
        for file in files:
            if file.endswith(".csv"):
                # print(file)
                df = pd.read_csv(
                    os.path.join(val_path, file),  # directory change to val
                    delimiter=',',
                    usecols=['Close', 'Volume']  # ['Date', 'Open', 'High', 'Low', 'Close', 'Volume'])
                )
                val_csv_list.append(df)
                if df.isnull().values.any():
                    print("Something is null in: {}".format(file))
                i += 1

    if len(val_csv_list) < 5 or len(train_csv_list) < 40:
        return train_csv_list, val_csv_list, 1

    return train_csv_list, val_csv_list, 0
