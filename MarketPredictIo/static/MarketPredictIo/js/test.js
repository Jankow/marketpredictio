$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
})

function ToggleLoadingWheel(option){
    if(option === 0) {
        if(document.forms['left_form'].reportValidity())
        {
            $("#loading-text").css("display", "block");
            $("#loading-text").html("Model creation and training in progress...");
            $("#loading-wheel").toggle();
        }
    }
    else if(option === 1) {
        if(document.forms['right_form'].reportValidity())
        {
            $("#loading-text").css("display", "block");
            $("#loading-text").html("Loading the model in progress...");
            $("#loading-wheel").toggle();
        }
    }
    else {
        $("#loading-text").css("display", "none");
        $("#loading-text").html("");
        $("#loading-wheel").toggle();
    }
}

function ToggleUploadOwnData(){
    if ($("#if-own-data").is(':checked')) {
        console.log("checked");
        $("#prepared-data-choice").css("display", "none");
        $("#own-data-choice").css("display", "block");
    }
    else
    {
        console.log("unchecked");
        $("#prepared-data-choice").css("display", "block");
        $("#own-data-choice").css("display", "none");
    }
}
