from pandas_datareader import data
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import urllib.request, json
import os
import numpy as np
import tensorflow as tf  # This code has been tested with TensorFlow 1.6
from tensorflow.python import keras
from sklearn.preprocessing import MinMaxScaler
import pydot
import graphviz
from IPython.display import SVG
from tensorflow.python.keras.utils import plot_model
# from keras.utils import model_to_dot

import PIL.Image
import io
from io import *
from django.shortcuts import render
from matplotlib import pylab
from pylab import *
import base64

from MarketPredictIo.helpers import CsvReader
from MarketPredictIo.helpers import ModelSaver


def teach(number_of_epochs, model_name, start_date, end_date):

    no_success = 0
    read_from_dir_success = 0
    read_prepared_success = 0
    csv_list_not_normalized = []
    val_list = []

    dir = os.path.dirname(__file__)
    print("Directory from StandardReluNN.py: " + dir + "")

    file = open('MarketPredictIo\\Data\\temp\\LastDate.txt', 'r')
    dates = file.read().split(" ")
    file.close()
    if start_date == dates[0] and end_date == dates[1]:
        print("Dates are the same as last time")
        print("Start date: {}".format(dates[0]))
        print("End date: {}".format(dates[1]))
        csv_list_not_normalized, val_list, read_prepared_success = CsvReader.read_prepared_data(
            "MarketPredictIo\\Data\\temp")
    else:
        print("Dates are different than the last time")
        print("Start date: {}".format(dates[0]))
        print("End date: {}".format(dates[1]))
        csv_list_not_normalized, val_list, read_from_dir_success = CsvReader.read_from_dir(
            "MarketPredictIo\\Data\\", start_date, end_date)
        if no_success == 0:
            file = open('MarketPredictIo\\Data\\temp\\LastDate.txt', 'w')
            file.write(str(start_date) + " " + str(end_date))
            file.close()

    if read_from_dir_success != 0:
        if read_from_dir_success == 1:
            return None, None, 4
        else:
            return None, None, 5

    if read_prepared_success != 0:
        if read_prepared_success == 1:
            return None, None, 4
        else:
            return None, None, 5

    if csv_list_not_normalized is None or val_list is None:
        return None, None, 5

    # csv_list_not_normalized = csv_list_not_normalized
    # val_list = csv_list_not_normalized

    print(csv_list_not_normalized[0])
    print(csv_list_not_normalized[0].iloc[-125:])
    print(csv_list_not_normalized[0].iloc[-125:, 0])
    print(csv_list_not_normalized[0].iloc[-125:-10, 0])
    print(csv_list_not_normalized[0].iloc[-125:, 1])
    print(csv_list_not_normalized[0].iloc[-125:-10, 1])
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print(np.asarray(csv_list_not_normalized[0].iloc[-125:, 0]))
    print(np.asarray(csv_list_not_normalized[0].iloc[-125:, 1]))
    print(np.asarray(csv_list_not_normalized[0].iloc[-125:-10, 0]).reshape(-1, 1))

    closes_and_volumes = []
    scaler = MinMaxScaler()
    i = 0
    while i < len(csv_list_not_normalized):
        # print("Wazne: " + str(csv_list_not_normalized[i].ix[:115, 0]))
        if len(np.asarray(csv_list_not_normalized[i].iloc[-125:-10, 0]).reshape(-1, 1)) == 0:
            print("Bug: " + str(np.asarray(csv_list_not_normalized[i].iloc[-125:-10, 0]).reshape(-1, 1)))
        else:
            print("Fine")
        scaler.fit(np.asarray(csv_list_not_normalized[i].iloc[-125:-10, 0]).reshape(-1, 1))
        closes = np.asarray(scaler.transform(np.asarray(csv_list_not_normalized[i].iloc[:, 0]).reshape(-1, 1)))
        # print("Wazne: " + str(csv_list_not_normalized[i].iloc[:115, 1]))
        scaler.fit(np.asarray(csv_list_not_normalized[i].iloc[-125:-10, 1]).reshape(-1, 1))
        volumes = np.asarray(scaler.transform(np.asarray(csv_list_not_normalized[i].iloc[:, 1]).reshape(-1, 1)))
        closes_and_volumes.append(np.concatenate([closes, volumes]))
        # print(closes_and_volumes[i])
        i += 1

    validation_closes_and_volumes = []
    scaler = MinMaxScaler()
    i = 0
    while i < len(val_list):
        scaler.fit(np.asarray(val_list[i].iloc[-125:-10, 0]).reshape(-1, 1))
        closes = np.asarray(scaler.transform(np.asarray(val_list[i].iloc[:, 0]).reshape(-1, 1)))
        scaler.fit(np.asarray(val_list[i].iloc[-125:-10, 1]).reshape(-1, 1))
        volumes = np.asarray(scaler.transform(np.asarray(val_list[i].iloc[:, 1]).reshape(-1, 1)))
        validation_closes_and_volumes.append(np.concatenate([closes, volumes]))
        i += 1

    # Linear(sequential) stack of layers const
    model = tf.keras.Sequential()

    # Define input layer
    model.add(tf.keras.layers.InputLayer(input_shape=(230,)))

    model.add(tf.keras.layers.Dense(
        units=140,
        kernel_initializer='VarianceScaling',
        activation='tanh'
    ))

    model.add(tf.keras.layers.Dense(
        units=70,
        kernel_initializer='VarianceScaling',
        activation='tanh'
    ))
    model.add(tf.keras.layers.Dense(
        units=35,
        kernel_initializer='VarianceScaling'
    ))
    model.add(keras.layers.LeakyReLU(alpha=0.3))
    model.add(tf.keras.layers.Dense(
        units=15,
        kernel_initializer='VarianceScaling',
        activation='tanh'
    ))
    model.add(tf.keras.layers.Dense(
        units=10,
        kernel_initializer='VarianceScaling'
    ))
    model.add(keras.layers.LeakyReLU(alpha=0.3))

    # SVG(model_to_dot(model).create(prog='dot', format='svg'))
    plot_model(model, os.path.join("MarketPredictIo\\Models\\Saved\\", model_name + '.png'),
               show_shapes=True)

    model.compile(loss='mae',
                  optimizer='adam',
                  metrics=['mse', 'mae', 'mape', 'cosine'])

    x_train = np.array([])
    y_train = np.array([])
    i = 0
    while i < len(closes_and_volumes):
        # x_train = np.append(x_train, closes_and_volumes[i][:115 i 125:240])
        x_train = np.append(x_train, np.asarray([closes_and_volumes[i][:115], closes_and_volumes[i][125:240]]).reshape(-1, 230))
        y_train = np.append(y_train, closes_and_volumes[i][115:125, 0])
        i += 1
    print(x_train[0])

    x_val = np.array([])
    y_val = np.array([])
    i = 0
    while i < len(validation_closes_and_volumes):
        x_val = np.append(x_val, np.asarray([validation_closes_and_volumes[i][:115], validation_closes_and_volumes[i][125:240]]).reshape(-1, 230))
        y_val = np.append(y_val, validation_closes_and_volumes[i][115:125, 0])
        i += 1

    x_train = x_train.reshape(-1, 230)
    y_train = y_train.reshape(-1, 10)

    x_val = x_val.reshape(-1, 230)
    y_val = y_val.reshape(-1, 10)

    print("x_train: " + str(len(x_train)) + ", y_train: " + str(len(y_train)))
    print("x_val: " + str(len(x_val)) + ", y_val: " + str(len(y_val)))

    history = model.fit(x_train, y_train,
                        batch_size=64,
                        epochs=number_of_epochs,
                        validation_data=(x_val, y_val),
                        steps_per_epoch=1
                        )

    test_scores = model.evaluate(x_train, y_train, verbose=2)

    print('Test loss:', test_scores[0])
    print('Test accuracy:', test_scores[1])

    ModelSaver.save(model, model_name)

    # print(history.history.keys())

    plt.close('all')
    # ax = plt.gca()
    # ax.set_facecolor((211.0/255.0, 211.0/255.0, 211.0/255.0))
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    buffer = io.BytesIO()
    plt.savefig(buffer, format='png', facecolor=(211.0/255.0, 211.0/255.0, 211.0/255.0))
    plot_graphic = buffer.getvalue()
    plot_graphic = base64.b64encode(plot_graphic)
    buffer.close()

    # plt.plot(history.history['cosine_proximity'])
    # plt.plot(history.history['val_cosine_proximity'])
    # plt.title('model cosine')
    # plt.ylabel('cosine')
    # plt.xlabel('epoch')
    # plt.legend(['train', 'val'], loc='upper left')
    # buffer = io.BytesIO()
    # plt.savefig(buffer, format='png')
    # plot_graphic = buffer.getvalue()
    # plot_graphic = base64.b64encode(plot_graphic)
    # buffer.close()

    image = PIL.Image.open(os.path.join("MarketPredictIo\\Models\\Saved\\", model_name + '.png'))
    buffer = io.BytesIO()
    image.save(buffer, format='png')
    model_graphic = buffer.getvalue()
    model_graphic = base64.b64encode(model_graphic)
    buffer.close()
    return model_graphic, plot_graphic, no_success


def generate_result(test_data_chosen, model_name, uploaded_data=None, if_validation_data=True):

    no_success = 0

    # directory = "MarketPredictIo\\Data\\test\\Stocks"
    # csv_list = []
    # i = 0
    # for root, dirs, files in os.walk(directory):
    #     for file in files:
    #         if file.endswith(".txt"):
    #             df = pd.read_csv(
    #                 os.path.join(directory, file),
    #                 delimiter=',',
    #                 usecols=['Date', 'Open', 'High', 'Low', 'Close', 'Volume'])
    #             df['Date'] = pd.to_datetime(df['Date'])
    #             # Choose data from given range of time
    #             start_date = '01-02-2016'
    #             end_date = '30-07-2016'
    #             mask = (df['Date'] > start_date) & (df['Date'] <= end_date)
    #             df = df.loc[mask]
    #             dfSize = len(df.index)
    #             # print(os.path.basename(file) + ": ")
    #             # # print(df.to_csv())
    #             # print(dfSize)
    #             if dfSize >= 125:
    #                 print(str(file) + " is correct")
    #                 correct_df = df.tail(125)
    #                 # csv_list.append(correct_df)
    #                 correct_df.to_csv("D:\\Projects\\MarketPredictIo\\MarketPredictIo\\Data\\temp\\test\\Stocks\\"
    #                                   + file.replace(".us.txt", "").upper() + ".csv")
    #             # print(len(csv_list))
    #             i += 1

    # for df in csv_list:
    #     print()
    #     df.to_csv("D:\\Projects\\MarketPredictIo\\MarketPredictIo\\Data\\temp\\test\\Stocks\\"
    #               + file.replace(".us.txt", "").upper() + ".csv")

    model = ModelSaver.load(model_name)
    model.compile(loss='mse',
                  optimizer='adam',
                  metrics=['mse', 'mae', 'mape', 'cosine'])

    df = []
    # if os.path.en(uploaded_data):

    if uploaded_data is not None:  # change to elif
        try:
            df = pd.read_csv(
                uploaded_data,
                delimiter=',',
                usecols=['Close', 'Volume']  # ['Date', 'Open', 'High', 'Low', 'Close', 'Volume'])
            )
        except:
            return [], [], 2
        df_size = len(df.index)
        if df_size < 115 or (if_validation_data and df_size < 125):
            print("why no return??")
            return [], [], 1
        if df.columns[0] != 'Close' or df.columns[1] != 'Volume':
            return [], [], 3
    else:
        try:
            df = pd.read_csv(
                os.path.join("D:\\Projects\\MarketPredictIo\\MarketPredictIo\\Data\\temp\\test\\Stocks\\",
                             test_data_chosen + ".csv"),
                delimiter=',',
                usecols=['Close', 'Volume']  # ['Date', 'Open', 'High', 'Low', 'Close', 'Volume'])
            )
        except:
            return [], [], 2
        df_size = len(df.index)
        if df_size < 115 or (if_validation_data and df_size < 125):
            return [], [], 1
        if df.columns[0] != 'Close' or df.columns[1] != 'Volume':
            return [], [], 3

    # scaler = MinMaxScaler()
    # scaler.fit(df[:115])
    # prices_and_volumes = scaler.transform(df)

    if if_validation_data:
        number_of_samples = 125
    else:
        number_of_samples = 115

    tab_end = 115 - number_of_samples

    prices_scaler = MinMaxScaler()
    volume_scaler = MinMaxScaler()
    if if_validation_data:
        prices_scaler.fit(df.iloc[-number_of_samples:tab_end, 0].values.reshape(-1, 1))
    else:
        prices_scaler.fit(df.iloc[-number_of_samples:, 0].values.reshape(-1, 1))
    close_prices = prices_scaler.transform(df.iloc[:, 0].values.reshape(-1, 1))
    if if_validation_data:
        volume_scaler.fit(df.iloc[-number_of_samples:tab_end, 1].values.reshape(-1, 1))
    else:
        volume_scaler.fit(df.iloc[-number_of_samples:, 1].values.reshape(-1, 1))
    volume_sizes = volume_scaler.transform(df.iloc[:, 1].values.reshape(-1, 1))

    # print(close_prices[-number_of_samples:].shape)
    # print(volume_sizes[-number_of_samples:].shape)
    if if_validation_data:
        concatenated_list = np.concatenate((close_prices[-number_of_samples:tab_end],
                                        volume_sizes[-number_of_samples:tab_end]))
    else:
        concatenated_list = np.concatenate((close_prices[-number_of_samples:],
                                        volume_sizes[-number_of_samples:]))
        # print(close_prices[-number_of_samples:])
        # print(volume_sizes[-number_of_samples:])

    # print(concatenated_list.shape)
    # print(concatenated_list)

    x_test = concatenated_list.reshape(-1, 230)

    print("x_test: " + str(x_test.shape))
    print(x_test)

    predictions = model.predict_on_batch(x_test)

    print(predictions.shape)
    print(predictions)

    # inverse transforming data to plot well
    predictions = prices_scaler.inverse_transform(predictions)
    x_test = prices_scaler.inverse_transform(x_test)

    print(predictions)
    x_plot = np.arange(115, 125, 1)

    plt.close('all')
    legend = []
    if if_validation_data:  # len(df) == 125 or
        plt.plot(df.iloc[-125:-10, 0].values.reshape(115, -1))
        plt.plot(x_plot, predictions.reshape(10, -1))
        y_test = np.array(close_prices[115:]).reshape(-1, 10)
        y_test = prices_scaler.inverse_transform(y_test)
        plt.plot(x_plot, y_test.reshape(10, -1))
        legend = ['main', 'prediction', 'reality']
    elif not if_validation_data:  # len(df) == 115 or
        plt.plot(df.iloc[-115:, 0].values.reshape(115, -1))
        plt.plot(x_plot, predictions.reshape(10, -1))
        legend = ['main', 'prediction']
    plt.title('predictions')
    plt.ylabel('predictions')
    plt.xlabel('days')
    plt.legend(legend, loc='upper left')
    buffer = io.BytesIO()
    plt.savefig(buffer, format='png', facecolor=(211.0/255.0, 211.0/255.0, 211.0/255.0))
    plot_graphic = buffer.getvalue()
    plot_graphic = base64.b64encode(plot_graphic)
    buffer.close()

    image = PIL.Image.open(os.path.join("MarketPredictIo\\Models\\Saved\\", model_name + '.png'))
    buffer = io.BytesIO()
    image.save(buffer, format='png')
    model_graphic = buffer.getvalue()
    model_graphic = base64.b64encode(model_graphic)
    buffer.close()
    return model_graphic, plot_graphic, no_success


