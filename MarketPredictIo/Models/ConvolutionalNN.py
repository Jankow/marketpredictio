from pandas_datareader import data
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import urllib.request, json
import os
import numpy as np
import tensorflow as tf  # This code has been tested with TensorFlow 1.6
from sklearn.preprocessing import MinMaxScaler

import PIL.Image
import io
from io import *
from django.shortcuts import render
from matplotlib import pylab
from pylab import *
import base64

from MarketPredictIo.helpers import CsvReader
from MarketPredictIo.helpers import ModelSaver


def teach():

    csv_list = CsvReader.read_prepared_data(
        "D:\\Projects\\MarketPredictIo\\MarketPredictIo\\Data\\temp\\train\\Stocks\\")

    # csv_list = CsvReader.read_from_dir(
    #     "D:\\Projects\\MarketPredictIo\\MarketPredictIo\\Data\\train\\Stocks")

    # Linear(sequential) stack of layers const
    model = tf.keras.Sequential()

    # Define input layer
    model.add(tf.keras.layers.InputLayer(input_shape=(115,)))

    # Add the first convolutional layer
    model.add(tf.keras.layers.Conv1D(
        kernel_size=2,
        filters=128,
        strides=1,
        use_bias=True,
        activation=tf.keras.activations.relu,
        # kernelInitializer='VarianceScaling'
    ))

    # Add the Average Pooling layer
    model.add(tf.keras.layers.AveragePooling1D(
        pool_size=[2],
        strides=[1]
    ))

    # Add the second convolutional layer
    model.add(tf.keras.layers.Conv1D(
        kernel_size=2,
        filters=64,
        strides=1,
        use_bias=True,
        activation=tf.keras.activations.relu,
        # kernelInitializer='VarianceScaling'
    ))

    # Add the Average Pooling layer
    model.add(tf.keras.layers.AveragePooling1D(
        pool_size=[2],
        strides=[1]
    ))

    # # Add Flatten layer, reshape input to(number of samples, number of features)
    # model.add(tf.keras.layers.Flatten(
    #     # inputs=
    # ))

    # Add Dense layer
    model.add(tf.keras.layers.Dense(
        units=10,
        kernel_initializer='VarianceScaling',
        activation='linear'
    ))

    model.compile(loss='mean_squared_error',
                  optimizer='adam',
                  metrics=['mse', 'mae', 'mape', 'cosine'])

    # SVG(model_to_dot(model).create(prog='dot', format='svg'))
    tf.keras.utils.plot_model(model, 'my_first_model.png', show_shapes=True)

    x_train = np.array([])
    y_train = np.array([])
    for csv in csv_list:
        x_train = np.append(x_train, csv[:115].values)
        y_train = np.append(y_train, csv[115:].values)

    x_train = x_train.reshape(-1, 115)
    y_train = y_train.reshape(-1, 10)

    index_of_list = 50
    print(csv_list[index_of_list][:115])
    print(csv_list[index_of_list][115:])

    print("Shape1: {}".format(x_train.shape))
    print("Shape2: {}".format(y_train.shape))

    history = model.fit(x_train, y_train,
                        batch_size=64,
                        epochs=1000,
                        validation_split=0.2,  # Change into validation_data, also needs batch_size/validation_steps
                        steps_per_epoch=1
                        )

    test_scores = model.evaluate(x_train, y_train, verbose=2)

    print('Test loss:', test_scores[0])
    print('Test accuracy:', test_scores[1])

    ModelSaver.save(model, "ConvolutionalNN")

    # print(history.history.keys())

    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    buffer = io.BytesIO()
    plt.savefig(buffer, format='png')
    plot_graphic = buffer.getvalue()
    plot_graphic = base64.b64encode(plot_graphic)
    buffer.close()

    # plt.plot(history.history['cosine_proximity'])
    # plt.plot(history.history['val_cosine_proximity'])
    # plt.title('model cosine')
    # plt.ylabel('cosine')
    # plt.xlabel('epoch')
    # plt.legend(['train', 'val'], loc='upper left')
    # buffer = io.BytesIO()
    # plt.savefig(buffer, format='png')
    # plot_graphic = buffer.getvalue()
    # plot_graphic = base64.b64encode(plot_graphic)
    # buffer.close()

    image = PIL.Image.open('D:\\Projects\\MarketPredictIo\\my_first_model.png')
    buffer = io.BytesIO()
    image.save(buffer, format='png')
    model_graphic = buffer.getvalue()
    model_graphic = base64.b64encode(model_graphic)
    buffer.close()
    return model_graphic, plot_graphic


def generate_result(test_data_chosen):

    directory = "D:\\Projects\\MarketPredictIo\\MarketPredictIo\\Data\\train\\Stocks"

    df = pd.read_csv(os.path.join(directory, 'hpq.us.txt'),
                     delimiter=',', usecols=['Date', 'Open', 'High', 'Low', 'Close'])  # , dtype={"data": "datetime"})
    print('Loaded data from the Kaggle repository')
    print(df.describe())

    csvList = []
    i = 0
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(".txt"):
                dataframe = pd.read_csv(
                    os.path.join(directory, file),
                    delimiter=',', usecols=['Date', 'Open', 'High', 'Low', 'Close'])  # , dtype={"data": "datetime"})
                # Choose data from given range of time

                csvList.append(dataframe)
                i += 1

    # Linear(sequential) stack of layers const
    model = tf.keras.Sequential()

    # Define input layer
    model.add(tf.keras.layers.InputLayer(input_shape=(7, 1)))

    # Add the first convolutional layer
    model.add(tf.keras.layers.Conv1D(
        kernel_size=2,
        filters=128,
        strides=1,
        use_bias=True,
        activation=tf.keras.activations.relu,
        # kernelInitializer='VarianceScaling'
    ))

    # Add the Average Pooling layer
    model.add(tf.keras.layers.AveragePooling1D(
        pool_size=[2],
        strides=[1]
    ))

    # Add the second convolutional layer
    model.add(tf.keras.layers.Conv1D(
        kernel_size=2,
        filters=64,
        strides=1,
        use_bias=True,
        activation=tf.keras.activations.relu,
        # kernelInitializer='VarianceScaling'
    ))

    # Add the Average Pooling layer
    model.add(tf.keras.layers.AveragePooling1D(
        pool_size=[2],
        strides=[1]
    ))

    # # Add Flatten layer, reshape input to(number of samples, number of features)
    # model.add(tf.keras.layers.Flatten(
    #     # inputs=
    # ))

    # Add Dense layer
    model.add(tf.keras.layers.Dense(
        units=1,
        kernel_initializer='VarianceScaling',
        activation='linear'
    ))

    # Finished building model
    # Model Compilation:
    model.compile(optimizer='adam', loss='mean_squared_error')

    # print(model.predict(x).shape)

    # Train model:

    # todo: train the model here
    # model.fit()

    # Test the model:
    # var predictedX = model.predict(tensorData.tensorTrainX)

    return

    # buffer = io.BytesIO()
    # plt.savefig(buffer, format='png')
    # graphic = buffer.getvalue()
    # graphic = base64.b64encode(graphic)
    # buffer.close()
    # return graphic
